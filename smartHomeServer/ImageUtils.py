import numpy as np
import face_recognition

PICTURE_WIDTH = 800
PICTURE_HEIGHT = 1200
ACCURACY_LEVEL = 0.4


'''
encode the face in order to compare it to other faces

@:param image: the image to encode

:return: the encoded image
'''
def encode_image(image):
    print("log: start encode image")

    # resize the image
    new_size = (PICTURE_WIDTH, PICTURE_HEIGHT)
    img = image.resize(new_size)

    # convert the image to openCV type
    open_cv_image = np.array(img)

    # change it to rgb
    rgb_image = open_cv_image[:, :, ::-1]
    final_img = rgb_image

    # get all the faces in the image
    faces = face_recognition.face_encodings(final_img)

    # check if the image has faces
    if len(faces) == 0:
        return None

    print("log: end encode image")

    return faces[0]


'''
check if 2 faces belong to the same person

@:param image: the image to check
@:param users_encodings: the encodings of all the others images of this user

:return: if the 2 faces belong to the same person
'''
def check_face(image, users_encodings):
    print("log: start check face")

    unknown_encoding = encode_image(image)

    if not unknown_encoding:
        return False

    # search go over all the faces in the new img
    for new_face in [unknown_encoding]:

        # search over all the existing users faces
        for known_face in users_encodings:
            print("[known_face]", [known_face])
            print("new_face", new_face)
            result = face_recognition.compare_faces([known_face], new_face, ACCURACY_LEVEL)
            print(result)

            # if face is found quit
            if True in result:
                return True

    return False













