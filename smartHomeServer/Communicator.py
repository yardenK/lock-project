import socket
import json

import ImageUtils

from _thread import *
import threading

from PIL import Image
import time
import io

from Database import Database


class Communicator:
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port

        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.bind((self.ip, self.port))
        self.server_socket.listen(5)

        self.connected_users_dict = {}
        self.mutex = threading.Lock()

        self.users_database = Database("users_db.p")

    '''
    start listen on the server socket for new client's 
    '''
    def run(self):

        print("server running")

        while True:

            new_socket, addr = self.server_socket.accept()
            print('Connected to :', addr[0], ':', addr[1])

            start_new_thread(self.handel_client, (new_socket,))

        s.close()

    '''
    run on other thread for each client that connected to the server 
    
    @:param socket: the socket of the client that communicating with the server in this thread
    '''
    def handel_client(self, socket):

        while True:

            try:

                # get the data
                data = socket.recv(1024).decode('utf-8')

                # remove 2 unwanted bytes in the start
                data = data[2:]

                print("log: data", data)

                # check if the socket has disconnected
                if not data:
                    break

                # handel the request
                self.handel_request(data, socket)

            except Exception as e:
                print(e)

        # after the socket disconnected or the program crushed remove the user form the connected users dict
        self.remove_user(socket)

        socket.close()

    '''
    get the request that the user sent and run the correct function to handel it
    
    @:param data: the request that the user sent
    @:param socket: the socket that sent the data
    '''
    def handel_request(self, data, socket):
        json_data = json.loads(data)

        state = json_data["state"]
        print("state", state)

        if state == "add_user":
            self.add_user(json_data, socket)

        elif state == "send_msg":
            self.send_msg(json_data)

        elif state == "add_image":
            self.add_image(json_data, socket)

        elif state == "check_image":
            self.check_image(json_data, socket)

    '''
    add new user to the connected users dict

    @:param json_data: the request that the user sent
    @:param socket: the socket that sent the request
    '''
    def add_user(self, json_data, socket):

        user_id = json_data["id"]
        user_data = {"socket": socket}

        self.mutex.acquire()
        self.connected_users_dict[user_id] = user_data
        self.mutex.release()

        print(self.connected_users_dict)

    '''
    remove that user from the connected users dict
    
    @:param socket: the socket of the client that need to be removed
    '''
    def remove_user(self, socket):

        self.mutex.acquire()

        # get the  user_id of the user with the given socket
        for user_key in self.connected_users_dict:
            if self.connected_users_dict[user_key]["socket"] is socket:

                # remove him
                print("log: user removed")
                self.connected_users_dict.pop(user_key)
                break

        self.mutex.release()

    '''
    pass request to the other user by his use id

    @:param json_data: the request that the user sent
    '''
    def send_msg(self, json_data):

        user_id = json_data["id"]
        user_data = json_data["data"]

        # check if the user is connected
        if user_id not in self.connected_users_dict.keys():
            return

        # get the socket of the user
        self.mutex.acquire()
        socket = self.connected_users_dict[user_id]["socket"]
        self.mutex.release()

        # send the msg to the user
        socket.send(user_data.encode())

    '''
    add image to the users data base

    @:param json_data: the request that the user sent
    @:param socket: the socket that sent he request
    '''
    def add_image(self, json_data, socket):
        print("log: start add")

        # get the image from the user
        image = self.get_image(json_data, socket)

        user_id = json_data["id"]

        # encode the image
        encode_image = ImageUtils.encode_image(image)

        # save the image
        if not encode_image:
            self.users_database.save_image(user_id, encode_image)

        print("log: end add")

    '''
    check if image is in the users database and if it belong to the correct user id

    @:param json_data: the request that the user sent
    @:param socket: the socket that sent he request
    '''
    def check_image(self, json_data, socket):
        print("log: start check image")

        # get image from the user
        image = self.get_image(json_data, socket)

        user_id = json_data["id"]

        # check the image
        users_images = self.users_database.get_images(user_id)

        print("log: end check image")

        print("log: res", ImageUtils.check_face(image, users_images))

        image.show()

    '''
    get image form the user

    @:param json_data: the request that the user sent
    @:param socket: the socket to get the image from
    '''
    def get_image(self, json_data, socket):

        print('log: start get image')

        image_bytes = bytearray()

        # get the length of the image
        length = json_data["data"]

        # read data from the socket till the data is in the length of the picture
        while len(image_bytes) < length:
            current_bytes = socket.recv(1024)
            image_bytes += bytearray(current_bytes)

        # convert the bytes to picture
        image1 = io.BytesIO(image_bytes)
        image = Image.open(image1).convert("RGB")

        print('log: end get image')

        return image












