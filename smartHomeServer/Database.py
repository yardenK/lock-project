import pickle


class Database:
    def __init__(self, path):

        self.path = path

        try:

            # load all users
            self.database = pickle.load(open(path, "rb"))

        except FileNotFoundError as e:
            self.database = dict()

    '''
    save image in the data base by its user id
    
    @:param user_id: th eid of the user to save 
    @:param image: the image to save
    '''
    def save_image(self, user_id, image):
        print("start save image")

        # self.database[str(user_id)] = []
        self.database[str(user_id)].append(image)
        print("database", self.database)

        pickle.dump(self.database, open(self.path, "wb"))

        print("end save image")

    def get_images(self, user_id):
        return self.database[str(user_id)]











