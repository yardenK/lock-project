package com.example.smarthouse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.StringWriter;

public class MainActivity extends AppCompatActivity {

    private static final int PICTURE_RESULT = 2;
    private static final int IMAGE_QUALITY = 50;

    private BottomNavigationView bottomNavigationView;
    private Fragment fragment = null;

    private ImageView button_on;
    private ImageView button_off;

    private Uri imageUri;
    private Bitmap bit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNavigationView = findViewById(R.id.bottomNav);
        bottomNavigationView.setOnNavigationItemSelectedListener(selectedListener);

        fragment = new HomeFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();

        new communicator(this).execute();


    }

    public void clickOpenCamera()
    {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        imageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, PICTURE_RESULT);

    }

    // called when the camera return a picture
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case PICTURE_RESULT:
                if (requestCode == PICTURE_RESULT)
                    if (resultCode == Activity.RESULT_OK) {
                        try {
                            JSONObject userRequest = new JSONObject();

                            // save the image
                            bit = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);

                            // send the image
                            ByteArrayOutputStream bout = new ByteArrayOutputStream();
                            bit.compress(Bitmap.CompressFormat.JPEG, IMAGE_QUALITY, bout);

                            userRequest.put("state", "check_image");
                            userRequest.put("id", new Integer(1));
                            userRequest.put("data", new Integer(bout.size()));

                            communicator.write(userRequest.toString());
                            Log.e("DataSendImg", "send img meta data");

                            communicator.write(bout.toByteArray());

                            Log.e("DataSendImg", "send image");

                        } catch (Exception e) {
                            Log.e("myErrorSenImg", e.toString());
                        }

                    }
        }

    }


    private BottomNavigationView.OnNavigationItemSelectedListener selectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId())
            {
                case R.id.home:
                    fragment = new HomeFragment();
                    break;

                case R.id.lock:
                    fragment = new LockFragment(MainActivity.this);
                    break;
            }

            getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
            return true;
        }
    };
}