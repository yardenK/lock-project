package com.example.smarthouse;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;

public class communicator extends AsyncTask<Void, Void, String> {

    static private Socket socket;
    static private DataOutputStream output;
    static private BufferedReader input;

    static private Activity activity;

    public communicator(Activity activity)
    {
        communicator.activity = activity;
    }

    @Override
    protected String doInBackground(Void... params) {

        String message = "";

        try
        {
            communicator.socket = new Socket("192.168.0.102", 12345);
            communicator.output = new DataOutputStream(socket.getOutputStream());
            communicator.input = new BufferedReader(new InputStreamReader(socket.getInputStream()));



            //Toast.makeText(communicator.activity, "connected", Toast.LENGTH_SHORT).show();
            Log.e("DataCreateSocket", "connected");
        }
        catch (Exception e)
        {
            Log.e("myErrorCreateSocket", e.toString());
        }


        while(true)
        {
            try {

                if (socket.getInputStream().available() != 0)
                {
                    message = input.readLine();
                    Log.e("DataReadSocket", message);

                }
            } catch (Exception e)
            {
                Log.e("myErrorReadSocket", e.toString());
            }
        }

    }

    static void write(final String text)
    {


            new Thread(new Runnable(){
                @Override
                public void run() {

                    try
                    {

                        communicator.output.writeUTF(text);
                        communicator.output.flush();

                    } catch (IOException e)
                    {
                        Log.e("myErrorSendSocket", e.toString());
                    }

                }
            }).start();
    }

    static void write(final byte[] bytes)
    {


        new Thread(new Runnable(){
            @Override
            public void run() {

                try
                {
                    Thread.sleep(100);
                    communicator.output.write(bytes);
                    Log.e("myErrorSendSocket", "aaa");


                } catch (Exception e)
                {
                    Log.e("myErrorSendSocket", e.toString());
                }

            }
        }).start();
    }

}
